import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { HomePageModule } from '../pages/home/home.module';
import { EndadduserPageModule } from '../pages/endadduser/endadduser.module';
import { MiperfilPageModule } from '../pages/miperfil/miperfil.module';
import { NavpagePageModule } from  '../pages/navpage/navpage.module';
import { NeweventPageModule } from '../pages/newevent/newevent.module';
import { NewuserPageModule } from '../pages/newuser/newuser.module';
import { PerfilesPageModule } from '../pages/perfiles/perfiles.module';
import { SearchusersPageModule } from '../pages/searchusers/searchusers.module';
import { DetallePageModule } from '../pages/detalle/detalle.module';
import { GooglesearchPageModule } from '../pages/googlesearch/googlesearch.module';
import { ConfiguracionesPageModule } from '../pages/configuraciones/configuraciones.module';
import { MyListPageModule } from '../pages/my-list/my-list.module';
import { PrivadosPageModule } from '../pages/privados/privados.module';
import { MyfollowersPageModule } from '../pages/myfollowers/myfollowers.module';
import { EstadisticasPageModule } from '../pages/estadisticas/estadisticas.module';
//providers

import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps } from "@ionic-native/google-maps";
import { HTTP } from '@ionic-native/http';
import { HttpProvider } from '../providers/http/http';
import { HttpClientModule } from '@angular/common/http';

import { AgmCoreModule } from '@agm/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    HomePageModule,
    EndadduserPageModule,
    MiperfilPageModule,
    NavpagePageModule,
    NeweventPageModule,
    NewuserPageModule,
    PerfilesPageModule,
    SearchusersPageModule,
    DetallePageModule,
    GooglesearchPageModule,
    ConfiguracionesPageModule,
    MyListPageModule,
    PrivadosPageModule,
    MyfollowersPageModule,
    EstadisticasPageModule,
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyAowbLQTqle8_R2zIHNYunzGIvsT9LR9ig',
    }),

    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    File,
    FileTransfer,
    FileTransferObject,
    Geolocation,
    HTTP,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpProvider,
    GoogleMaps,
  ]
})
export class AppModule {}
