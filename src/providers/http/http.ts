import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import {App} from "ionic-angular";
import { EndadduserPage } from '../../pages/endadduser/endadduser';
//import { ConfigurationsPage } from '../../pages/configurations/configurations';
 /* Generated class for the HttpProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HttpProvider {

  constructor(public http: HttpClient , 
              public toastCtrl : ToastController , 
              public loadingCtrl: LoadingController,
              public alertCtrl : AlertController,
              public app : App,  ) {
    
  }


  datos = {}
  urlUser : any;
  sendpostToLogin(datos, url, options){
     //envio por post
     return new Promise(resolve => {
         this.http.post(url, datos, options).subscribe(data=>{
           resolve(data);

            
         }, err => {
           let toast = this.toastCtrl.create({
              message: 'Usuario o contraseña Incorrectos',
              duration: 3000,
              position: 'middle'
            });
            toast.present();
 
         });
     });
      
  }


  getUserData(url,options){
    return new Promise(resolve => {
      this.http.get(url, options).subscribe(data=>{
        resolve(data);
      }, err=>{

      });
    });
  }

  getAllUserData(url,options, data){
    return new Promise(resolve => {
      this.http.get(url, options).subscribe(datos=>{
        resolve(datos);
      }, err=>{

      let alert  = this.alertCtrl.create({
          title : 'Bienvenido a Join' ,
          message : 'Para comenzar necesitamos que termines de completar la informacion que a continuacion se te solicitara luego de eso podras compartir y ver todos los eventos que quieras a Disfrutar!',
          buttons : [
          {
            text : 'Completar Informacion',
            handler : () =>{


               let nav = this.app.getActiveNav();
                nav.push(EndadduserPage, {
                item : data ,
                item2 : options ,
              });
     
            }
          }]
        });

        alert.present();
        
      });
    });
  }


  error_not_found(){

  }

  getAllUserPublication(url, options){
      let http = this.http.get(url, options)
      return http
  }



  newUser(datos, url, options){
    return new Promise(resolve => {
      this.http.post(url, datos, options).subscribe(data=>{
        resolve(data);
      }, err => {
        console.log(JSON.stringify(err.error))
        let toast = this.toastCtrl.create({
              message : 'ups porfavor revisa lo siguientes errores' + JSON.stringify(err.error),
              duration: 10000,
              position: 'top'
            });
          toast.present();
      });
    });
  }




 updateUser(datos, url, options){
   return new Promise(resolve => {
      this.http.put(url, datos, options).subscribe(data=>{
        resolve(data);
      }, err => {
        console.log(JSON.stringify(err));
        let toast = this.toastCtrl.create({
               message: 'Hubo un problema actualizando',
              duration: 3000,
              position: 'top'
            });
          toast.present();
      });
    });
 }




 log_out(url, options){
   return new Promise(resolve => {
     this.http.post(url, options).subscribe(data=>{
       resolve(data);
     }, err => {
       
     })
   })
 }




 nuevoevento(url, datos, options){
    return new Promise(resolve => {
      this.http.post(url, datos, options).subscribe(data=>{
        resolve(data);
         let toast = this.toastCtrl.create({
              message : 'Evento creado correctamente' ,
              duration: 10000,
              position: 'top'
            });
          toast.present();

      }, err => {
        console.log(JSON.stringify(err.error))
        let toast = this.toastCtrl.create({
              message : 'ups porfavor revisa lo siguientes errores' + JSON.stringify(err.error),
              duration: 10000,
              position: 'top'
            });
          toast.present();
      });
    })
 }


 nuevoFollower(url,datos,options){

    return new Promise(resolve => {
      this.http.post(url, datos, options).subscribe(data=>{
        resolve(data);
         let toast = this.toastCtrl.create({
              message : 'Siguiendo usuario' ,
              duration: 10000,
              position: 'top'
            });
          toast.present();

      }, err => {
        console.log(JSON.stringify(err.error))
        let toast = this.toastCtrl.create({
              message : 'ups porfavor revisa lo siguientes errores' + JSON.stringify(err.error),
              duration: 10000,
              position: 'top'
            });
          toast.present();
      });
    })


 }


 searchFollower(url,datos,options){

   return new Promise(resolve => {
     this.http.post(url, datos, options).subscribe(data=>{
       resolve(data);
     },err=>{
       return err
     });
   })
 }



 delete_data(url, options){
   return new Promise(resolve => {
     this.http.get(url, options).subscribe(data=>{
       resolve(data);
     }, err => {
       
     })
   })
 }


update(url, datos , options){
  return new Promise(resolve=>{
      this.http.put(url, datos, options)
    .subscribe(Result=>{
      resolve(Result);

       let toast = this.toastCtrl.create({
              message : 'Listo!' ,
              duration: 1000,
              position: 'top'
            });
          toast.present();
    },err=>{
      console.log(err);
       let toast = this.toastCtrl.create({
              message : 'Listo!' ,
              duration: 1000,
              position: 'top'
            });
          toast.present();
    })
  })
  
}



categoria(url, datos, options){
  return new Promise(resolve =>{
    this.http.post(url, datos, options)
      .subscribe(result=>{
        resolve(result)
      }, err =>{
        console.log(err);
      })
  })
}


new_follow(url, datos, options){
  return new Promise(resolve => {
    this.http.post(url, datos, options)
      .subscribe(Result => {
        resolve(Result)
      }, err => {
        console.log(err);
      })
  })
}


search_followers(url, options){
  return new Promise(resolve=>{
     this.http.get(url,options)
       .subscribe(res =>{
         resolve(res)
       }, err => {
         console.log(err);
       })
  })
    
}


search_myfollower(url, options){
  return new Promise(resolve=>{
    this.http.get(url, options)
      .subscribe(res => {
        resolve(res)
      }, err => {
        console.log(err);
      })
  })
}


}
