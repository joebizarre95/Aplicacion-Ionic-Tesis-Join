import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';

import { DomSanitizer } from '@angular/platform-browser';
import { DetallePage } from '../detalle/detalle';
/**
 * Generated class for the PerfilesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-perfiles',
  templateUrl: 'perfiles.html',
})
export class PerfilesPage {
	id : any;
	token : any;
	datos = {};
	data = {};
	result : {};
	user = {};
  usuario = {};
  headers : any;
  data3 = {}
  data2 = {}
  resultado = {}
  constructor(public navCtrl: NavController,
                 public navParams: NavParams , 
                     public httpProvider : HttpProvider, 
                        public sanitizer : DomSanitizer) {

  		this.id =  this.navParams.get('item');
  		this.token = this.navParams.get('token');
  		this.user = this.navParams.get('data');

      console.log(this.id);
      console.log(this.token['key']);
      console.log(this.user);



  }

  ionViewDidLoad() {

    this.headers = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.token['key']
            },
        };


      let urls = 'http://apijoin.pythonanywhere.com/api/getUserDetail/' + this.id;
        this.httpProvider.getAllUserData(urls, this.headers , this.data)
          .then((res)=>{
            let data = res;
            console.log(data);
            this.data2 = res
            let url = 'http://apijoin.pythonanywhere.com/api/GetPublicaciones/' + this.id;
              this.httpProvider.getAllUserPublication(url, this.headers)
                .subscribe((data)=>{
                  console.log(data);
                  this.result = data;
                }, err=>{
                  console.log(err);
                })
          }, err => {
            console.log(err);
          })

  }





  volver(){
  	this.navCtrl.pop();
  }


   detalle(id){
   this.navCtrl.push(DetallePage, {
        data : id,
        key : this.token['key']
    });
  }

  

}
