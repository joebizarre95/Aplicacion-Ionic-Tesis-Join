import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { ToastController } from 'ionic-angular';


import { DomSanitizer } from '@angular/platform-browser';
import { AlertController } from 'ionic-angular';
import { MyfollowersPage } from '../myfollowers/myfollowers';
import { EstadisticasPage } from '../estadisticas/estadisticas';

/**
 * Generated class for the ConfiguracionesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-configuraciones',
  templateUrl: 'configuraciones.html',
})
export class ConfiguracionesPage {

	//variables
	item: any;
	item2: any;
	datos = {};


  constructor(public navCtrl: NavController, 
  					public navParams: NavParams,
  						public httpProvier : HttpProvider,
  							public toastCtrl : ToastController, 
  								public sanitizer : DomSanitizer,
  									public alertCtrl: AlertController,) {

  	this.item = this.navParams.get('item');
  	this.item2= this.navParams.get('item2');

  }



  ionViewDidLoad() {
    console.log(this.item); //datos del usuario

    console.log(this.item2);//key

    let data = {}

    var headers = {
    	headers: {
    		'Content-Type':'application/json',
    		'Accept':'application/json',
    		'Authorization':'token ' + this.item2['key']
    	},
    };


    let url = 'http://apijoin.pythonanywhere.com/api/getUserDetail/' + this.item['user']
    this.httpProvier.getAllUserData(url, headers, data)
    	.then((result)=>{
    		if(result === 200){
    			console.log('error');
    		}else{

    			this.datos = result

    			console.log(this.datos);

    			let toast = this.toastCtrl.create({
    				message : 'Panel de control Cargado',
    				duration: 2000,
    				position: 'top',

    			});

    			toast.present();
    		}
    	}, err =>{
    		console.log(err);
    	});





  }


  datos2 = {

  	apellido: this.datos['apellido'],
  	bio: this.datos['bio'],
  	fecha_nacimiento : this.datos['fecha_nacimiento'],
  	nombre: this.datos['nombre'],
  	ubicacion: this.datos['ubicacion'],
  	user: this.datos['user']

  }


  actualizar(){
  	

    var headers = {
    	headers: {
    		'Content-Type':'application/json',
    		'Accept':'application/json',
    		'Authorization':'token ' + this.item2['key']
    	},
    };

  		this.datos2.apellido = this.datos['apellido'];
    	this.datos2.bio = this.datos['bio'];
    	this.datos2.fecha_nacimiento = this.datos['fecha_nacimiento'];
    	this.datos2.nombre = this.datos['nombre'];
    	this.datos2.ubicacion = this.datos['ubicacion'];
    	this.datos2.user = this.datos['user'];

    	let url = 'http://apijoin.pythonanywhere.com/api/update/' + this.item['user'] 

    	this.httpProvier.update(url,this.datos2,headers)
    		.then((success)=>{
    			
    		let toas = this.toastCtrl.create({
    			message:'Datos Actualizados',
    			position:'Top',
    			duration:2000

    			})	;

    		toas.present();

    		}, err=>{
    			console.log(err);


    		let toas = this.toastCtrl.create({
    			message:'Listo!',
    			position:'Top',
    			duration:2000

    			})	;

    		toas.present();
    		})
  }

  mas(){

  	 const alert = this.alertCtrl.create({
      title: 'Bienvenido a Join!',
      subTitle: 'Join es una aplicacio elaborada por Santiago Sanchez conocido como Mono , un desarrollador independiente que tuvo la idea de crearme en su casa!',
      buttons: ['Volver']
    });
    alert.present();

  }

  reporte(){

  	 const prompt = this.alertCtrl.create({
      title: 'Reporte de Errores',
      message: "Envia un pequeño texto explicando el error",
      inputs: [
        {
          name: 'TEXTO',
          placeholder: '...'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();
  }


  myfollowers(){

    this.navCtrl.push(MyfollowersPage,{
      token : this.item2['key'],
      id: this.item['user']
    })

  }


  estadisticas(){
      this.navCtrl.push(EstadisticasPage,{
      token : this.item2['key'],
      id: this.item['user']
    })
  }

  



}
