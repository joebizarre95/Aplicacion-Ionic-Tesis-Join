import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { AlertController } from 'ionic-angular';


/**
 * Generated class for the MyfollowersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-myfollowers',
  templateUrl: 'myfollowers.html',
})
export class MyfollowersPage {
	id: any;
	token: any;
	data : {};
  constructor(public navCtrl: NavController, public navParams: NavParams , public http : HttpProvider ,public alertCtrl: AlertController) {
  	this.id = this.navParams.get('id');
  	this.token = this.navParams.get('token');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyfollowersPage');
    console.log(this.id);
    console.log(this.token);


     console.log('ionViewDidLoad MyListPage');
    console.log(this.id);
    console.log(this.token);

    	var headers = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.token
            },
        };
		let url = 'http://apijoin.pythonanywhere.com/api/invitados/' + this.id;
                this.http.search_followers(url, headers)
                	.then((success)=>{
                		console.log(success);
                		this.data = success;
                	}, err => {
                		console.log(err);
                	});



                	 const alert = this.alertCtrl.create({
					      title: 'Hey!',
					      subTitle: 'cargando!',
					      buttons: ['OK']
					    });
					    alert.present();
					  



  }

  dejar_de_seguir(id){

  		let datos = {
  		follower : id,
  		user : this.id, 
  	}

  	console.log(datos);


  	let url = 'http://apijoin.pythonanywhere.com/api/deleteFollower/';

  	var headers = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.token
            },
        };

  	this.http.new_follow(url, datos, headers)
  		.then((success)=>{

  			this.ionViewDidLoad();
  			console.log(success);

  		}, err=>{
  			console.log(err);
  		})


  }

}
