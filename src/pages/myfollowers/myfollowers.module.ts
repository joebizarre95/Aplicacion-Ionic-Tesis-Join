import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyfollowersPage } from './myfollowers';

@NgModule({
  declarations: [
    MyfollowersPage,
  ],
  imports: [
    IonicPageModule.forChild(MyfollowersPage),
  ],
})
export class MyfollowersPageModule {}
