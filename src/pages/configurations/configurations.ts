import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
//provider
import { HttpProvider } from '../../providers/http/http';
//import { ImagePicker } from '@ionic-native/image-picker';
import { Camera, CameraOptions } from '@ionic-native/camera';
/**
 * Generated class for the ConfigurationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-configurations',
  templateUrl: 'configurations.html',
})
export class ConfigurationsPage {

photo : any;
value: any;
value2: any;
datos =  {}
picked_image : string
id: any;



  constructor(public navCtrl: NavController,
  				public navParams: NavParams ,
  				public httpProvider: HttpProvider,
  				public toastCtrl : ToastController ,
          //public imagePiker : ImagePicker,
          public camera : Camera ) {
  	this.value = navParams.get('item');
  	this.value2 = navParams.get('item2');
    this.id = navParams.get('id');
  }



  ionViewDidLoad() {
   /* let data = {}
    console.log(this.value);
    console.log(this.value2);
    var headers = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.value2['key'] //el token pasa a ser value 2
            },
        };
    let url = 'http://127.0.0.1:8000/api/getUserDetail/' + this.value['user'];
    this.httpProvider.getAllUserData(url,headers,data).then(
    	(result)=>{
    		if(result === 200){
    			//error :v
    		}else{
    			this.datos = result
    			let toast = this.toastCtrl.create({
    				message : 'Panel de edicion cargado',
    				duration: 3000,
    				position: 'middle',
    			});
    			toast.present();
    		}
    	});*/
  }



  data = {}
  update(){
    let pk = this.value['user']

    let datos = {
      data : this.data ,
      imagen : 'urldelaimagen',
    }

    let url = 'http://127.0.0.1:8000/api/UpdateDataRequest/'+ pk
    var headers = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.value2['key'] //el token pasa a ser value 2
            },
        };

        this.httpProvider.updateUser(datos, url, headers).then(
          (response)=>{
            console.log(JSON.stringify(response));
          }).catch((error) => {
            console.log(JSON.stringify(error));
          });

      }//fin de la funcion




     /*pickimage(){
       var options = {
          maximumImagesCount: 1,
          width: 500,
          height: 500,
          quality: 50,
          outputType: 1
       };

       this.imagePiker.getPictures(options)
         .then((results) => {
           this.picked_image = 'data:image/jpeg;base64' + results[0];

         }, (err : any) => {
           console.log(err)
           let toast = this.toastCtrl.create({
             message : 'ups hubo un error' + JSON.stringify(err),
             duration: 4000,
             position: 'top',
           });

           toast.present();
         })

     }*/

     pick_photo(){
       const options : CameraOptions = {
         quality: 1000,
         destinationType : this.camera.DestinationType.DATA_URL,
         encodingType: this.camera.EncodingType.JPEG,
         mediaType : this.camera.MediaType.PICTURE
       }

       this.camera.getPicture(options)
         .then((imageData) => {
           let base64Image = 'data:image/jpeg;base64' + imageData;

           this.photo = base64Image;

           let toast  = this.toastCtrl.create({
             message : 'imagen seleccionada ' + JSON.stringify(this.photo),
             position: 'top',
             duration: 3000,

           })

           toast.present();

         }).catch((error)=>{

           let toast  = this.toastCtrl.create({
             message : 'Error' + JSON.stringify(error),
             position: 'top',
             duration: 3000,

           })

           toast.present();
         })

     }

}
