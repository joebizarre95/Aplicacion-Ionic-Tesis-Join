import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

//provider
import { HttpProvider } from '../../providers/http/http';

/**
 * Generated class for the NewuserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-newuser',
  templateUrl: 'newuser.html',
})
export class NewuserPage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams ,
              public httpProvider: HttpProvider, 
               public toastCtrl : ToastController ,) {
  	
  }

  ionViewDidLoad() {
    
  }


  dismiss(){
  	this.navCtrl.pop();
  }

  datos = { username:null, password1:null, password2:null, email:null ,}
  registration(){
    console.log(this.datos);
    let datos = this.datos
    let url = 'http://apijoin.pythonanywhere.com/api/LoginAPI/registration'
     var headers = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
            },
        };
    this.httpProvider.newUser(datos, url, headers).then(
      (response)=>{
        if(response === 200){
          console.log('hubo un eror ');
        }else{

             let toast = this.toastCtrl.create({
                 message: 'Perfecto! Todo salio como esperaba!',
                duration: 2000,
                position: 'middle'
              });
            

             toast.onDidDismiss(() => {
              this.navCtrl.pop();
          });

             toast.present();
          }
         
      });
  }

}
