import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


//provider
import { HttpProvider } from '../../providers/http/http';



import { DomSanitizer } from '@angular/platform-browser';

import { DetallePage } from '../detalle/detalle';

import { AlertController } from 'ionic-angular';

import { LoadingController } from 'ionic-angular';



/**
 * Generated class for the MiperfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-miperfil',
  templateUrl: 'miperfil.html',
})
export class MiperfilPage {
 
  constructor(public navCtrl: NavController,
              public navParams: NavParams ,
               public httpProvider: HttpProvider,
                  public sanitizer : DomSanitizer,
                    public alertCtrl: AlertController,
                      public loadingCtrl: LoadingController,
              ) {

  		this.value = navParams.get('item'); //informacion del usuario
  		this.value2 = navParams.get('item2');//token
  		this.value3 = navParams.get('item3');//data del usuario del modelo principal de django el user

  }
  value : any;
  value2 : any;
  value3 : any;
  data = {};
  data2 = {};
  data3 = {};
  resultado = {};
  result : {};
  autenticacion : any;
  headers = {}
  ionViewDidLoad() {
  	this.data = this.value;  //informacion personal
  	this.data2 = this.value3; //informacion user
  	//consultar los eventos referentes a ti
    this.headers = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.value2['key']
            },
        };

       let url = 'http://apijoin.pythonanywhere.com/api/LoginAPI/user/';
       this.httpProvider.getUserData(url,this.headers)
       .then((rest)=>{
           let data = rest;
           this.data3 = rest;
           let url = 'http://apijoin.pythonanywhere.com/api/getUserDetail/' + this.data['pk'] ;
           this.httpProvider.getAllUserData(url, this.headers , data)
             .then((success)=>{
              this.data2 = success
              let url = 'http://apijoin.pythonanywhere.com/api/GetPublicaciones/' + this.data['pk'] ;
              this.httpProvider.getAllUserPublication(url, this.headers)
              .subscribe((data)=>{
                  console.log(data);
                  this.result = data;
              }, err =>{
                  console.log(err);
              })

             })   
         console.log('usuario encontrado');
         this.resultado = rest
        },err => {
          console.log(err)
          console.log('usuario no encontrado');
        });


  }


  recargar(){

      this.data = this.value;  //informacion personal
    this.data2 = this.value3; //informacion user
    //consultar los eventos referentes a ti
    this.headers = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.value2['key']
            },
        };

       let url = 'http://apijoin.pythonanywhere.com/api/LoginAPI/user/';
       this.httpProvider.getUserData(url,this.headers)
       .then((rest)=>{
           let data = rest;
           this.data3 = rest;
           let url = 'http://apijoin.pythonanywhere.com/api/getUserDetail/' + this.data['pk'] ;
           this.httpProvider.getAllUserData(url, this.headers , data)
             .then((success)=>{
              this.data2 = success
              let url = 'http://apijoin.pythonanywhere.com/api/GetPublicaciones/' + this.data['pk'] ;
              this.httpProvider.getAllUserPublication(url, this.headers)
              .subscribe((data)=>{
                  console.log(data);
                  this.result = data;
              }, err =>{
                  console.log(err);
              })

             })   
         console.log('usuario encontrado');
         this.resultado = rest
        },err => {
          console.log(err)
          console.log('usuario no encontrado');
        });



  }

  volver(){
    this.navCtrl.pop();
  }

  detalle(id){
   this.navCtrl.push(DetallePage, {
        data : id,
        key : this.value2['key'],
    });
  }


  eliminar(id){

    console.log(this.value2['key']);


    const confirm = this.alertCtrl.create({
      title: 'Deseas Eliminar?',
      message: 'Se eliminara la publicacion de tu lista y de todas las listas del sistema',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Eliminar',
          handler: () => {

             this.recargar();

              const loader = this.loadingCtrl.create({
                      content: "Espere un momento porfavor...",
                      duration: 5000
                    });
                 
                    loader.present();

                    
               let url = 'http://apijoin.pythonanywhere.com/api/deletePublication/' + id;
              this.httpProvider.delete_data(url, this.headers)
              .then((data)=>{
                


                  

              }, err =>{
                  console.log(err);
              })

          }
        }
      ]
    });
    confirm.present();

  }




}
