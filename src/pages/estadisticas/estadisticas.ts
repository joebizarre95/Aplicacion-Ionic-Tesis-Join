import { Component , ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { Chart } from 'chart.js';
import { LoadingController } from 'ionic-angular';
/**
 * Generated class for the EstadisticasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-estadisticas',
  templateUrl: 'estadisticas.html',
})
export class EstadisticasPage {

	 @ViewChild('barCanvas') barCanvas;
	 @ViewChild('barCanvas2') barCanvas2;

	id: any;
	token: any;
	data: any;
	barChart: any;
	barChart2: any;
	data2: any;


	evento_1_3 = 0;
	evento_4_6 = 0;
	evento_7_9 = 0;
	evento_10_12 = 0;


	follow_1_3 = 0;
	follow_4_6 = 0;
	follow_7_9 = 0;
	follow_10_12 = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public httpProvider : HttpProvider, public loadingCtrl: LoadingController) {
  	  	this.id = this.navParams.get('id');
  		this.token = this.navParams.get('token');
  }

  ionViewDidLoad() {

  	 const loader = this.loadingCtrl.create({
      content: "Cargando Estadisticas...",
      duration: 5000
    });
    loader.present();
  


    console.log('ionViewDidLoad EstadisticasPage');

     var headers = {
    	headers: {
    		'Content-Type':'application/json',
    		'Accept':'application/json',
    		'Authorization':'token ' + this.token
    	},
    };


     let url = 'http://apijoin.pythonanywhere.com/api/GetPublicaciones/' + this.id ;
              this.httpProvider.search_myfollower(url, headers)
              .then((data)=>{


              	this.data = data;
              	for(var item of this.data){
              		if(item['fecha_evento'] <= '2018-03-31' ){
                  			let one = 1
                  			this.evento_1_3 += one
               		   }else{
                  			if(item['fecha_evento'] <= '2018-06-31'){
                  				let one = 1
                  				this.evento_4_6 += one
                  			}else{
                  					if(item['fecha_evento'] <= '2018-09-31'){
                  						let one = 1
                  						this.evento_7_9 += one
                  					}else{
                  							if(item['fecha_evento'] <= '2018-12-31'){
                  								let one = 1
                  								this.evento_10_12 += one
                  							}else{
                  								}
                  						}

                  				}

                  			}

                  			this.mychar(this.evento_1_3, this.evento_4_6, this.evento_7_9, this.evento_10_12);
       					
                  }
                  


                  	let urls = 'http://apijoin.pythonanywhere.com/api/invitados/' + this.id
                  	this.httpProvider.getAllUserPublication(urls, headers)
                  		.subscribe((datas)=>{
                  			this.data2 = datas
                  			for(var item2 of this.data2){
                  				//console.log(item2)
                  				var fecha = item2['follower_date'];

                  				if(fecha <= '2018-03-31 12:59:59.999999'){
                  					let one = 1 ;
                  					this.follow_1_3 += one
                  				}else{
                  					if(fecha <= '2018-06-31 12:59:59.999999'){
                  						let one = 1;
                  						this.follow_4_6 += one

                  					}else{

                  						if(fecha <= '2018-09-31 12:59:59.99999'){
                  							let one = 1;
                  							this.follow_7_9 += one 
                  						}else{

                  							if(fecha <= '2018-12-31 12:59:59.99999'){
                  								let one =1 ;

                  								this.follow_10_12 += one

                  							}

                  						}


                  					}

                  				}

                  				  this.mychardos(this.follow_1_3, this.follow_4_6 , this.follow_7_9 , this.follow_10_12);

                  			}
                  		}, err => {
                  			console.log(err);
                  		})



              }, err =>{
                  console.log(err);
              });


            

        
  }



  mychar(ev1,ev2,ev3,ev4){
  	  this.barChart = new Chart(this.barCanvas.nativeElement, {
 
            type: 'bar',
            data: {
                labels: ["1-3", "4-6", "7-9", "10-12",],
                datasets: [{
                    label: 'Eventos por Intervalo de Meses',
                    data: [ev1, ev2, ev3, ev4],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 0.2)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
 
        });
 
  	
  }

  mychardos(ev1, ev2, ev3 ,ev4){


  	  this.barChart2 = new Chart(this.barCanvas2.nativeElement, {
 
            type: 'bar',
            data: {
                labels:  ["1-3", "4-6", "7-9", "10-12",],
                datasets: [{
                    label: 'Eventos por Intervalo de Meses',
                    data: [ev1, ev2, ev3, ev4,],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                         'rgba(75, 192, 192, 0.2)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
 
        });
 
  	
  }

}
