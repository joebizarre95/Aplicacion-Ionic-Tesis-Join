import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Headers, RequestOptions } from '@angular/http';
import { ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';


//provider
import { HttpProvider } from '../../providers/http/http';


//pages
import { NavpagePage } from '../navpage/navpage';
import { NewuserPage } from '../newuser/newuser';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController ,
               public httpProvider: HttpProvider,
               public toastCtrl: ToastController,
                public loadingCtrl: LoadingController,
                  public modalCtrl: ModalController) {

  }

  datos = { username:null,
              password:null,}
  status = {}
  signing(){
  	  	 var headers = new Headers();
       headers.append("Accept",'application/json');
       headers.append('Content-Type', 'application/x-www-form-urlencoded');
       //options
    	 let options = new RequestOptions({ headers : headers });
    	 let datos = this.datos
    	 let urlpost = 'http://apijoin.pythonanywhere.com/api/LoginAPI/login/'
    	  this.httpProvider.sendpostToLogin(datos, urlpost, options).then(
         (response)=>{
           if(response === 200){
             console.log('aqui iria que devolvio bien pero este sistema esta loco');
           }else{
             //aqui iria devolvio false pero comoe ste sistema esta loco
           // console.log(response);//vendria siendo el key
             let loading = this.loadingCtrl.create({
               content: 'Cargando Datos',
             });

             loading.present();

              setTimeout(() => {
                  loading.dismiss();
                  this.navCtrl.push(NavpagePage , {
                    item : response , //pasando a la pagina 2 el token
                  });

                }, 5000);
           }
         })
  }



  singout(){
      let modal = this.modalCtrl.create(NewuserPage);
      modal.present();
  }

}
