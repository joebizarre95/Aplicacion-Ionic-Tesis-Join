import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchusersPage } from './searchusers';

@NgModule({
  declarations: [
    SearchusersPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchusersPage),
  ],
})
export class SearchusersPageModule {}
