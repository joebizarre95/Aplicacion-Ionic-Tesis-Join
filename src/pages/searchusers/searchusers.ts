import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { PerfilesPage } from '../perfiles/perfiles';

import { DomSanitizer } from '@angular/platform-browser';

import { ActionSheetController } from 'ionic-angular';

import { LoadingController } from 'ionic-angular';

import { DetallePage } from '../detalle/detalle';

import { ToastController } from 'ionic-angular';


/**
 * Generated class for the SearchusersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()  
@Component({
  selector: 'page-searchusers',
  templateUrl: 'searchusers.html',
})
export class SearchusersPage {
	id : any;
	token : any;
	data = {};
	datos = {};
  event = {};
  options = {};
  result : {};
  user = {}
  followers = {};
  follower = {}
  constructor(public navCtrl: NavController,
  				 public navParams: NavParams,
  				 	public http: HttpProvider,
             public sanitizer : DomSanitizer,
               public actionSheetCtrl: ActionSheetController, 
                 public loadingCtrl: LoadingController , 
                   public toastCtrl: ToastController) {

  	this.id = navParams.get('id');
  	this.token = navParams.get('value');
    this.user = navParams.get('data');


  }

  ionViewDidLoad() {
    console.log(this.user);
    let url = 'http://apijoin.pythonanywhere.com/api/mis_seguidores/' + this.user['pk'];
      this.options = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.token['key']
            },
        };

       this.http.search_followers(url, this.options)
         .then((succes)=>{
           console.log(succes);
           this.followers = succes
           this.follower = succes[0]['follower'];
           console.log(this.follower)
         }, err => {
           console.log(err);
         })

 
  }

  items: string[];
  getItems(ev: any) {

    // set val to the value of the searchbar
    let val = ev.target.value;
    this.data = {
    	user : val
    }
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
     	//val actualmente tiene el valor de la busqueda 
     	let url = 'http://apijoin.pythonanywhere.com/api/search/';
     	this.options = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.token['key']
            },
        };

        this.http.searchFollower(url, this.data , this.options)
        	.then((success)=>{
            let data = success
            let pk : any;

            if(data[0]['categoria']){

              pk = data[0]['pk']
              this.event = data[0];
              console.log('evento encontrado');


            }else{


              pk = data[0]['pk']
               console.log('es un usuario');
               let url = 'http://apijoin.pythonanywhere.com/api/getUserDetail/' + pk;
                this.http.getAllUserData(url, this.options , data)
                  .then((result)=>{
                    this.data = data[0];
                    this.datos = result;
                      console.log('usuario encontrado');
                     console.log(this.datos);
                  });
              }

              
		        },err => {
		          console.log(err)
              console.log('no se encontro nada');
		          //console.log('usuario no encontrado');
        });

    }
  }

  por_categoria(id){
    console.log(id);

      this.options = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.token['key']
            },
        };

      const loader = this.loadingCtrl.create({
                content: "Buscando...",
                duration: 3000
              });
              loader.present();

              let data = {
                categoria : id
              }

              let url = 'http://apijoin.pythonanywhere.com/api/categoria/';

              this.http.categoria(url, data, this.options)
                .then((result)=>{

                  console.log(result);
                  this.result = result

                },err=>{

                  console.log(err);

                })
}


  ver_perfil(id){
    this.navCtrl.push(PerfilesPage, {
      item : id,
      token : this.token,
      data : this.data,
    })
  }


  detalle(id){
     this.navCtrl.push(DetallePage, {
        data : id,
        key : this.token['key'],
    });
  }


 


  categorias(){
     const actionSheet = this.actionSheetCtrl.create({
      title: 'Elije tu Categoria',
      buttons: [
        {
          text: 'Deportes',
          handler: () => {

            this.por_categoria(1)

          }
        },

        {
          text: 'Musica',
          handler: () => {

             this.por_categoria(2)


          }
        },

        {
          text: 'Adultos',
          handler: () => {

             this.por_categoria(3)

          }
        },

        {
          text: 'Ferias',
          handler: () => {

             this.por_categoria(4)


          }
        },

        {
          text: 'Comida',
          handler: () => {

             this.por_categoria(5)


          }
        },

        {
          text: 'Teatro',
          handler: () => {

             this.por_categoria(6)

                  }
        },

        {
          text: 'Fiestas',
          handler: () => {

             this.por_categoria(7)

          }
        },

        {
          text: 'Reuniones',
          handler: () => {
             this.por_categoria(8)
          }
        },

        {
          text: 'Familiar',
          handler: () => {
             this.por_categoria(9)

          }
        },

        {
          text: 'Coches',
          handler: () => {
             this.por_categoria(10)
          }
        },

          {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }


  seguir(id){

    let url = 'http://apijoin.pythonanywhere.com/api/comprobar_seguidor';

    let data = {
      user : this.user['pk'],
      follower : id,
    }

      this.options = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.token['key']
            },
        };


   this.http.new_follow(url, data, this.options)
       .then((succes)=>{
         console.log(succes);
         if(succes == 200){
          
             let toast = this.toastCtrl.create({
               message:'ya estas siguiendo a este usuario',
               position:'top',
               duration:2000,
             });

             toast.present();

         }else{


             let url = 'http://apijoin.pythonanywhere.com/api/seguidor/'

     let data = {
       user : this.user['pk'], 
       follower : id, 

     }

     console.log(data);

     this.options = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.token['key']
            },
        };


     this.http.new_follow(url, data, this.options)
       .then((succes)=>{
         console.log(succes);
       }, err => {
         console.log(err);
       });

         }
       }, err => {
         console.log(err);
       }); 

 

    /*
     let url = 'http://apijoin.pythonanywhere.com/api/seguidor/'

     let data = {
       user : this.user['pk'], 
       follower : id, 

     }

     console.log(data);

     this.options = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.token['key']
            },
        };


     this.http.new_follow(url, data, this.options)
       .then((succes)=>{
         console.log(succes);
       }, err => {
         console.log(err);
       }); */

  }





}
