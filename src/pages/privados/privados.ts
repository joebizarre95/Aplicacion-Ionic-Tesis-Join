import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { DetallePage } from '../detalle/detalle';

/**
 * Generated class for the PrivadosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-privados',
  templateUrl: 'privados.html',
})
export class PrivadosPage {
	id : any;
	token: any;
	datos : {};
  constructor(public navCtrl: NavController, public navParams: NavParams , public http : HttpProvider) {
  	this.id = this.navParams.get('userid');
  	this.token = this.navParams.get('token')

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrivadosPage');
    

     let headers = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.token
            },
        };

       let url = 'http://apijoin.pythonanywhere.com/api/eventos_privados_mios/' + this.id;
       this.http.getUserData(url,headers)
       .then((rest)=>{
       		console.log(rest);
       		this.datos = rest;
        },err => {
          console.log(err)
          console.log('usuario no encontrado');
        });
  }


  detalle(id){
  	this.navCtrl.push(DetallePage, {
        data : id,
        key : this.token,
    });
  }

}
