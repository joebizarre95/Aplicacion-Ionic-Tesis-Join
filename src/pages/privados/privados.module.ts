import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrivadosPage } from './privados';

@NgModule({
  declarations: [
    PrivadosPage,
  ],
  imports: [
    IonicPageModule.forChild(PrivadosPage),
  ],
})
export class PrivadosPageModule {}
