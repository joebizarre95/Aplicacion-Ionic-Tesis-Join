import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';
import { HttpProvider } from '../../providers/http/http';
import { ModalController } from 'ionic-angular';
import { MyListPage } from '../my-list/my-list';

import { AlertController } from 'ionic-angular';

declare var google;

@Component({
  selector: 'page-newevent',
  templateUrl: 'newevent.html'
})
export class NeweventPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  user: any;
  desde: any;
  hasta: any;
  fixcenter :boolean = true;

  values : any;
  value2: any;
  value3 :any;
  image_picker : any;
  valor : any;
  infowindow : any;

  latitud : any;
  longitud: any;




  constructor(public navCtrl: NavController,
                 public geolocation: Geolocation, 
                   public navParams: NavParams , 
                      public httpProvider: HttpProvider,
                        public modal : ModalController ,
                          public alert : AlertController,) {
      this.values = navParams.get('item');
    this.value2 = navParams.get('item2');
    this.value3 = navParams.get('item3');
    console.log(this.values);
    console.log(this.value2);
    console.log(this.value3);

    this.valor = this.values;
     
  }

  ionViewDidLoad(){
    //this.loadMap();
    this.findMe();
  }

  nuevoevento(){

  

    console.log(this.datos);
    this.datos.user = this.valor['pk']
    let datos = this.datos
    let url = 'http://apijoin.pythonanywhere.com/api/NewPublicacion'

      var options = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.value3['key']
            },
        };

    this.httpProvider.nuevoevento(url, datos, options)

    let urls = 'http://apijoin.pythonanywhere.com/api/GetLastPublic/' +  this.valor['pk']
      this.httpProvider.getUserData(urls, options)
        .then((res)=>{
          if(res[0]['privado'] == 1){

            console.log('este evento es privado');

             this.navCtrl.pop();

            const modal = this.modal.create(MyListPage , {

              id : this.valor['pk'],
              token : this.value3['key'],
              evento : res[0]['pk'],

            });

              modal.present();

          }else{
            console.log('este evento es publico');
            this.navCtrl.pop();
          }
        }, err=>{
          console.log(err);
        })

    //this.navCtrl.pop();

  }
 
  loadMap(){
 
    //let latLng = new google.maps.LatLng(3.3441473, -76.54564); //
    let latLng = new google.maps.LatLng(3.418428,-76.5376663); //Boxihome
 
    let mapOptions = {
      center: latLng,
      zoom: 18,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
 
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    
    this.addUser(latLng);
  }

  findMe(){

    this.geolocation.getCurrentPosition().then((position) => {
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      
         let mapOptions = {
           center: latLng,
           zoom: 20,
           mapTypeId: google.maps.MapTypeId.ROADMAP,
         }
      
         this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
         this.addUser(latLng);
    }, (err) => { this.loadMap(); } );

  }

  addUser(position){
    var image = 'http://maps.google.com/mapfiles/ms/micons/man.png';
    this.user = new google.maps.Marker({
      position: position,
      map: this.map,
      icon: image
    });

    this.desde = this.user.getPosition().lat() + "," +this.user.getPosition().lng();
   // this.updateUser();

    //Listeners del mapa
    this.map.addListener("click",(e)=>{

      let marker  = new google.maps.Marker({
        position: {lat:e.latLng.lat(),lng:e.latLng.lng()},
        map: this.map
        
      });
      this.hasta = marker.getPosition().lat() + "," +marker.getPosition().lng();
    
      //console.log(e.latLng.lat());
      //console.log(e.latLng.lng());

      this.latitud = e.latLng.lat();
      this.longitud =e.latLng.lng();

      this.datos.lat = e.latLng.lat();
      this.datos.long = e.latLng.lng()


    });
  }


 datos = {
    // user :
    user : null,
    titulo : null,
    fecha_evento : null,
    descripcion : null,
    categoria : null,
    lat: 0,
    long: 0,
    privado: 0,


  }

  updateUser(){

    
    setInterval(()=>{
      this.geolocation.getCurrentPosition().then((position) => {
        
              let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
              //console.log("lat: "+latLng.lat+" , "+latLng.lng);
              this.user.setPosition(latLng);
              this.desde = this.user.getPosition().lat() + "," +this.user.getPosition().lng();

              if(this.fixcenter){
                this.map.setCenter(latLng);

              }
             

            }, (err) => { this.loadMap(); } );
    },1000);
/*
    var that = this;
    setInterval(function() {
    
    that.geolocation.getCurrentPosition().then((position) => {

      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      console.log("lat: "+latLng.lat+" , "+latLng.lng);
      that.user.setPosition(latLng);

    }, (err) => { that.loadMap(); } );
    }, 1000);
    */
  }

  calcularRuta(){

    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;
    directionsDisplay.setMap(this.map);
    directionsService.route({
      origin: this.desde,
      destination: this.hasta,
      travelMode: 'DRIVING'
    }, function(response, status) {
        console.log(response);
        if (status === 'OK') {
          directionsDisplay.setDirections(response);
          this.fixcenter=false;
       
       
        } else {
          window.alert('Directions request failed due to ' + status);
        }
    });


  }

  addMarker(animation){
    
    let effect: any;

    switch (animation) {
      case "DROP":
        effect = google.maps.Animation.DROP;
        break;
      case "BOUNCE":
        effect = google.maps.Animation.BOUNCE;
        break;
      default:
        effect = null;
        break;
    }

     let marker = new google.maps.Marker({
       map: this.map,
       animation: effect,
       position: this.map.getCenter()
     });
    
     let content = "<p>Le Marker!</p>";          
    
     let infoWindow = new google.maps.InfoWindow({
        content: content
      });
    
      google.maps.event.addListener(marker, 'click', () => {
        infoWindow.open(this.map, marker);
      });



    
   }


   evento_privado(){
     console.log('evento_privado');

      const confirm = this.alert.create({
      title: 'Deseas crear un evento privado?',
      message: 'si aceptas entonces podras invitar solo a los usuarios quienes acudiran al evento',
      buttons: [
        {
          text: 'No, Volver',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Si',
          handler: () => {

            this.datos.privado = 1;

            /*
            const modal = this.modal.create(MyListPage , {
              id : this.valor['pk'],
              token : this.value3['key'],
            });
              modal.present();*/
          }
        }
      ]
    });
    confirm.present();

    
   }
}
