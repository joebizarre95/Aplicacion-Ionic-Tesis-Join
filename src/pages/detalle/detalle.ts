import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the DetallePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;

@IonicPage()
@Component({
  selector: 'page-detalle',
  templateUrl: 'detalle.html',
})
export class DetallePage {

@ViewChild('map') mapElement: ElementRef;
  map: any;
  user: any;
  desde: any;
  hasta: any;
  fixcenter :boolean = true;

	id: any;
	key: any;


	lat: any;
	long: any;



  constructor(public navCtrl: NavController, public navParams: NavParams, public httpProvider : HttpProvider , public geolocation: Geolocation,) {

  	this.id = this.navParams.get('data');
  	this.key = this.navParams.get('key');
  	  }

        result = {};

  ionViewDidLoad() {

  	this.findMe();

  	

		
    
  }

  loadMap(){
 
    //let latLng = new google.maps.LatLng(3.3441473, -76.54564); //
    let latLng = new google.maps.LatLng(this.lat,this.long); //Boxihome
 
    let mapOptions = {
      center: latLng,
      zoom: 20,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
 
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    
    //this.addUser(latLng);
  }

  findMe(){

    var headers = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.key
            },
        };


    let url = 'http://apijoin.pythonanywhere.com/api/getMyPublish/' + this.id ;
              this.httpProvider.getAllUserPublication(url, headers)
              .subscribe((data)=>{

                  this.result = data;
                  this.lat = data['lat']
                  this.long = data['long']

                  console.log(this.result);

                  this.geolocation.getCurrentPosition().then((position) => {
      let latLng = new google.maps.LatLng(data['lat'], data['long']);
      
         let mapOptions = {
           center: latLng,
           zoom: 20,
           mapTypeId: google.maps.MapTypeId.ROADMAP,
         }
      
         this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
         this.addUser(latLng);
    }, (err) => { this.loadMap(); } );






              }, err =>{
                  console.log(err);
              })


    

  }

  addUser(position){
    var contentString = 'Ubicacion del Evento';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });


    var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';

    var marker = new google.maps.Marker({
    position: position,
    map: this.map,
    title: 'Uluru (Ayers Rock)',
     mapTypeId: google.maps.MapTypeId.ROADMAP,
  });
  marker.addListener('click', function() {
    infowindow.open(this.map, marker);
  });



  }



  updateUser(){

   

  }

  calcularRuta(){

    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;
    directionsDisplay.setMap(this.map);
    directionsService.route({
      origin: this.desde,
      destination: this.hasta,
      travelMode: 'DRIVING'
    }, function(response, status) {
        console.log(response);
        if (status === 'OK') {
          directionsDisplay.setDirections(response);
          this.fixcenter=false;
       
       
        } else {
          window.alert('Directions request failed due to ' + status);
        }
    });


  }

  addMarker(animation){
   
    
   }

}
