import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeguidoresPage } from './seguidores';

@NgModule({
  declarations: [
    SeguidoresPage,
  ],
  imports: [
    IonicPageModule.forChild(SeguidoresPage),
  ],
})
export class SeguidoresPageModule {}
