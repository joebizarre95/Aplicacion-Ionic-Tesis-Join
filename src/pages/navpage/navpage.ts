import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { Http, Headers, RequestOptions } from '@angular/http';
//provider
import { HttpProvider } from '../../providers/http/http';
import { ToastController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';

//pages 
import { ConfigurationsPage } from '../configurations/configurations';
import { ConfiguracionesPage } from '../configuraciones/configuraciones';
import { MiperfilPage } from '../miperfil/miperfil';
import { NeweventPage } from '../newevent/newevent';
import { SearchusersPage } from '../searchusers/searchusers';
import { GooglesearchPage } from '../googlesearch/googlesearch';

import { DomSanitizer } from '@angular/platform-browser';

import { DetallePage } from '../detalle/detalle';
import { PerfilesPage } from '../perfiles/perfiles';
import { PrivadosPage } from '../privados/privados';
/**
 * Generated class for the NavpagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-navpage',
  templateUrl: 'navpage.html',
})
export class NavpagePage {
	value : any; 
	datos = {};
  id : any;
  obj = {};
  publish : {};
  constructor(public navCtrl: NavController, 
              public navParams: NavParams , 
               public httpProvider: HttpProvider,
                 public toastCtrl : ToastController ,
                   public menuCtrl: MenuController, 
                     public sanitizer : DomSanitizer ) {
  	this.value = navParams.get('item'); //token
  }
  data = {}
  data2 = {}
  
  ionViewDidLoad() {
       
 }

 ionViewWillEnter(){

     var headers = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.value['key']
            },
        };
       let url = 'http://apijoin.pythonanywhere.com/api/LoginAPI/user/';
       this.httpProvider.getUserData(url,headers)
       .then((rest)=>{
           let data = rest;
           this.id = rest['pk'];
           let url = 'http://apijoin.pythonanywhere.com/api/getUserDetail/' + rest['pk'];
           this.httpProvider.getAllUserData(url, headers , data)
             .then((success)=>{
              this.data = success
             // console.log(this.data)
              let url = 'http://apijoin.pythonanywhere.com/api/mis_seguidores/' + rest['pk'];
                this.httpProvider.search_myfollower(url, headers)
                  .then((success)=>{
                     this.obj = success;
                            for(let data of this.obj){
                              //console.log(data['follower']);
                              let url = 'http://apijoin.pythonanywhere.com/api/seguidor_publiacion/' + data['follower']
                                  this.httpProvider.search_myfollower(url, headers) // para buscar publicaciones usando el mismo metodo porque me dio flojera programar otro
                                    .then((success)=>{
                                       console.log(success);
                                       this.publish = success;
                                    }, err => {
                                      console.log(err);
                                    });
                            }
                  }, err => {
                    console.log(err);
                  });
             });   
         console.log('usuario encontrado');
         this.datos = rest
         console.log(this.datos);
        },err => {
          console.log(err)
          console.log('usuario no encontrado');
        });

 }


   configurations(){
      this.navCtrl.push(ConfiguracionesPage, {
        item : this.data ,
        item2 : this.value, 
      });

    }



    log_out(){
      let url = 'http://apijoin.pythonanywhere.com/api/LoginAPI/logout/'

      var headers = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.value['key']
            },
        };

      this.httpProvider.log_out(url, headers)
        .then((response) => {
          this.navCtrl.popToRoot();
          console.log('deslogueado');
        }).catch(err =>{
          console.log(err)
        });
    }


   miperfil(){
     this.navCtrl.push(MiperfilPage, {
       item : this.datos,
       item2 : this.value, 
       item3 : this.data2,
     });
   }

   nuevoEvento(){
     this.navCtrl.push(NeweventPage, {
       item : this.datos, //usuariodata
       item2 : this.data2, //userdata 
       item3 : this.value,
       id : this.id,
     })
   }

   buscar_usuario(){
     this.navCtrl.push(SearchusersPage, 
       {
         data: this.datos,
         id : this.id,
         value : this.value,
       })
   }


   search(){
     this.navCtrl.push(GooglesearchPage, {

       key : this.value,
     });
   }

   detalle(id){
   this.navCtrl.push(DetallePage, {
        data : id,
        key : this.value['key'],
    });
  }

  privados(){
    this.navCtrl.push(PrivadosPage, {
      token : this.value['key'],
      userid : this.id,
    });
  }


 



}
