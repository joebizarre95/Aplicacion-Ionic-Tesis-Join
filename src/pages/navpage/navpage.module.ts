import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NavpagePage } from './navpage';

@NgModule({
  declarations: [
    NavpagePage,
  ],
  imports: [
    IonicPageModule.forChild(NavpagePage),
  ],
})
export class NavpagePageModule {}
