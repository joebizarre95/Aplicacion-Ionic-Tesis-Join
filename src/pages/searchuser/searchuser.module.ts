import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchuserPage } from './searchuser';

@NgModule({
  declarations: [
    SearchuserPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchuserPage),
  ],
})
export class SearchuserPageModule {}
