import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { AlertController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the MyListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-list',
  templateUrl: 'my-list.html',
})
export class MyListPage {
	id : any
	key: any
	data : {};
	evento: any
  constructor(public navCtrl: NavController, 
  					public navParams: NavParams , 
  						public http: HttpProvider, 
  							public alertCtrl: AlertController,
  								public toastCtrl: ToastController,) {
  this.id =	this.navParams.get('id');
  this.key = this.navParams.get('token');
  this.evento = this.navParams.get('evento');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyListPage');
    console.log(this.id);
    console.log(this.key);

    	var headers = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.key
            },
        };
		let url = 'http://apijoin.pythonanywhere.com/api/invitados/' + this.id;
                this.http.search_followers(url, headers)
                	.then((success)=>{
                		console.log(success);
                		this.data = success;
                	}, err => {
                		console.log(err);
                	});



                	 const alert = this.alertCtrl.create({
					      title: 'Hey!',
					      subTitle: 'Ya que elejiste que tu evento sera privado, porfavor procede a enviar las invitaciones a tus seguidores y luego finaliza para completar la creacio de tu evento!',
					      buttons: ['OK']
					    });
					    alert.present();
					  
  }

  agregar(id){

  	let datos = {
  		user : id,
  		publicacion : this.evento
  	}

  	console.log(datos);


  	let url = 'http://apijoin.pythonanywhere.com/api/NewPrivado/';

  	var headers = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.key
            },
        };

  	this.http.new_follow(url, datos, headers)
  		.then((success)=>{

  			 const toast = this.toastCtrl.create({
			      message: 'Invitacion Enviada',
			      duration: 1000,
			      position:'top',
			    });
			    toast.present();

  		}, err=>{
  			console.log(err);
  		})


  }	


  finalizar(){
  	this.navCtrl.pop();
  }

}
