import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GooglesearchPage } from './googlesearch';

@NgModule({
  declarations: [
    GooglesearchPage,
  ],
  imports: [
    IonicPageModule.forChild(GooglesearchPage),
  ],
})
export class GooglesearchPageModule {}
