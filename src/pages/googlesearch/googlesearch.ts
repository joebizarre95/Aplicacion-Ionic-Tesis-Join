import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { Geolocation } from '@ionic-native/geolocation';
import { DetallePage } from '../detalle/detalle';

import { AlertController } from 'ionic-angular';

/**
 * Generated class for the GooglesearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;


@IonicPage()
@Component({
  selector: 'page-googlesearch',
  templateUrl: 'googlesearch.html',
})
export class GooglesearchPage {

@ViewChild('map') mapElement: ElementRef;
  map: any;
  user: any;
  desde: any;
  hasta: any;
  fixcenter :boolean = true;
  key: any;
  radiomax: any;
  longmax: any;
  radiomax2: any;
  longmax2: any;
  lat: any;
  long: any;
  data: any;
  data2: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public httpProvider : HttpProvider , public geolocation: Geolocation, public alertCtrl: AlertController) {

    this.key = this.navParams.get('key');
  }


 ionViewDidLoad() {

  	this.findMe();

    console.log(this.key);

  	

		
    
  }

  loadMap(){
   console.log('loadmap');
  }



  datos = {};

  findMe(){
    this.geolocation.getCurrentPosition().then((position) => {
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      
         let mapOptions = {
           center: latLng,
           zoom: 20,
           mapTypeId: google.maps.MapTypeId.ROADMAP,
         }

         //console.log(position.coords.latitude);
         //console.log(position.coords.longitude);

         this.lat = position.coords.latitude;
         this.long = position.coords.longitude;



          this.radiomax = position.coords.latitude + 0.001;
         //console.log(this.radiomax);

         this.longmax = position.coords.longitude - 0.001;
         //console.log(this.longmax);


         this.radiomax2 = position.coords.latitude - 0.001;

         this.longmax2 = position.coords.longitude + 0.001;
      

       this.datos = new google.maps.LatLng(this.radiomax , this.longmax)

        
         this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
         this.addUser(latLng);
    }, (err) => { this.loadMap(); } );

  }



                 /*
                       this.navCtrl.push(DetallePage, {
                                       // data : item['pk'],
                                        key : this.key['key'],
                                    });
                                */


   ver(){
       console.log('ver')
   }

  addUser(position){

    let headers = {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization' : 'token ' + this.key['key']
            },
        };

    let data = {
        lat : this.radiomax,
        long: this.longmax,
        mylat : this.lat,
        mylong: this.long,
    }

    let data2 = {

        lat : this.radiomax2,
        long: this.longmax2,
        mylat : this.lat,
        mylong: this.long,

    }


      let url = 'http://apijoin.pythonanywhere.com/api/rangos/';
              this.httpProvider.searchFollower(url, data, headers)
              .then((data)=>{
                this.data = data
                  for(var item of this.data){

                     console.log(item);

                     let latLng = new google.maps.LatLng(item['lat'], item['long']);
                     let infowindows = new google.maps.InfoWindow({
                       content: item['titulo'] + '<br>' +
                                item['descripcion'] + '<br>' +
                                item['fecha_evento'],
                     });

                      var image = 'http://maps.google.com/mapfiles/ms/micons/info_circle.png';

                      let marker = new google.maps.Marker({
                          position: latLng,
                          map: this.map,
                          title: 'Evento',
                          mapTypeId: google.maps.MapTypeId.ROADMAP,
                          icon: image
                      })

                      marker.addListener('click', function() {
                            infowindows.open(this.map, marker);
                          });

                  } //termina el for



                  let url = 'http://apijoin.pythonanywhere.com/api/rangos/'
                    this.httpProvider.searchFollower(url, data2, headers)
                      .then((data)=>{
                        this.data2 = data
                          for(var item of this.data2){
                              console.log('triangule');
                            console.log(item);

                             let latLng = new google.maps.LatLng(item['lat'], item['long']);
                             let infowindows = new google.maps.InfoWindow({
                               content: item['titulo'] + '<br>' +
                                        item['descripcion'] + '<br>' +
                                        item['fecha_evento'],
                             });

                              var image = 'http://maps.google.com/mapfiles/ms/micons/info_circle.png';

                              let marker = new google.maps.Marker({
                                  position: latLng,
                                  map: this.map,
                                  title: 'Evento',
                                  mapTypeId: google.maps.MapTypeId.ROADMAP,
                                  icon: image
                              })

                              marker.addListener('click', function() {
                                    infowindows.open(this.map, marker);
                                  });

                                  }
                      })

              }, err =>{
                  console.log(err);
              })


    var contentString = 'Tu Ubicacion Actual';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });


    var imagen = 'http://maps.google.com/mapfiles/ms/micons/man.png';
    var image = 'http://maps.google.com/mapfiles/ms/micons/info_circle.png';

    var marker = new google.maps.Marker({
    position: position,
    map: this.map,
    title: 'Tu posicion actual',
     mapTypeId: google.maps.MapTypeId.ROADMAP,
     icon: imagen
  });

     marker.addListener('click', function() {
    infowindow.open(this.map, marker);
  });




     var marker2 = new google.maps.Marker({
    position: this.datos,
    map: this.map,
    title: 'Tu posicion actual',
     mapTypeId: google.maps.MapTypeId.ROADMAP,
     icon: image
  });


      var contentString2 = 'Ubicacion de ejemplo';

     var infowindow2 = new google.maps.InfoWindow({
    content: contentString2
  });

     marker2.addListener('click', function() {
    infowindow2.open(this.map, marker2);
  });



 



  }


  ver_evento(data){
     console.log('ver evento');
     console.log(data);
   }



  updateUser(){

   

  }

  calcularRuta(){

    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;
    directionsDisplay.setMap(this.map);
    directionsService.route({
      origin: this.desde,
      destination: this.hasta,
      travelMode: 'DRIVING'
    }, function(response, status) {
        console.log(response);
        if (status === 'OK') {
          directionsDisplay.setDirections(response);
          this.fixcenter=false;
       
       
        } else {
          window.alert('Directions request failed due to ' + status);
        }
    });


  }

  addMarker(animation){
   
    
   }


  

}
