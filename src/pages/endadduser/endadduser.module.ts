import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EndadduserPage } from './endadduser';

@NgModule({
  declarations: [
    EndadduserPage,
  ],
  imports: [
    IonicPageModule.forChild(EndadduserPage),
  ],
})
export class EndadduserPageModule {}
