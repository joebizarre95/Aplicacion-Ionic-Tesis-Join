import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { ImagePicker } from '@ionic-native/image-picker';
import { ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { AlertController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';

//providers

import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

/**
 * Generated class for the EndadduserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-endadduser',
  templateUrl: 'endadduser.html',
})
export class EndadduserPage {
	value : any;
   pk : any;
   options : any;
   ImageData: string;

  constructor(public navCtrl: NavController,
                public navParams: NavParams,
                 // public imagePiker : ImagePicker,
                    public toastCtrl : ToastController ,
                      public http : HttpClient,
                        public alert : AlertController,
                          private transfer: FileTransfer,
                            private camara : Camera,
                              private file : File,
                   ) {

  	this.value = navParams.get('item');

    this.options = navParams.get('item2');

  }

  ionViewDidLoad() {
    console.log(this.value);//data del usuario
     let pk =  this.value['pk']; //primary key
     this.pk = pk;
  }

  picked_image : string
  camera(){
    const options : CameraOptions = {
      quality:30,
      destinationType : this.camara.DestinationType.DATA_URL,
      encodingType: this.camara.EncodingType.JPEG,
      mediaType: this.camara.MediaType.PICTURE,
      targetHeight: 200,
      targetWidth: 200,
    };

    this.camara.getPicture(options)
      .then((picture)=>
      {
          let base64image = 'data:image/jpeg;base64,'+ picture;
          this.picked_image = base64image;       
           this.ImageData = base64image;

      }, (err)=>{
        console.log(err);
      });
  }





      data = {
        user : null,
        ubicacion : null ,
        fecha_nacimiento : null,
        nombre : null,
        apellido : null,
        bio : null,
      }



     sendData(){

       this.data.user = this.pk;
       console.log(this.data);
       let url = 'http://apijoin.pythonanywhere.com/api/edAddUser/';
       this.http.post(url, this.data , this.options)
         .subscribe((sucess) => {
           console.log(sucess);
          let toast =  this.toastCtrl.create({
             message : 'su informacion esta siendo almacenada espere un poco...',
             duration : 5000,
             position : 'middle',
           });
           toast.present();
            
             this.navCtrl.pop();

         }, err => {
           console.log(err);

            let toast =  this.toastCtrl.create({
             message : 'comprueba tu conexion de red!' + JSON.stringify(err),
             duration : 10000,
             position : 'middle',
           });
           toast.present();

         });

          
        

     }


     


     uploadFile(file){

                       let fileTransfer : FileTransferObject = this.transfer.create();
                        let url = 'http://apijoin.pythonanywhere.com/api/edAddUser/';
                        let options : FileUploadOptions = {
                          fileKey: 'file',
                          fileName: this.picked_image,
                          headers: this.options
                        }
                       fileTransfer.upload(this.ImageData, url , options)
                           .then((data)=>{

                               let toast = this.toastCtrl.create({
                                    message : 'Imagen Guardada',
                                    position: 'middle',
                                    duration : 2000
                                  });

                                  toast.present();
                           }, err => {

                              let toast = this.toastCtrl.create({
                                    message : 'hubo un error' + JSON.stringify(err),
                                    position: 'middle',
                                    duration : 10000
                                  });

                                  toast.present();

                           });

     }




}
